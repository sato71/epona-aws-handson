#!/bin/bash -eu

SCRIPT_DIR=$(cd $(dirname $0); pwd)
REPOSITORY_ROOT=$(git rev-parse --show-toplevel)

USERS="$@"
. ${SCRIPT_DIR}/common.sh

check_tools

RUNTIME_INSTANCES_1=$(cat ${SCRIPT_DIR}/sequences/delete.runtime.sequence.1 | grep -v '^#')
RUNTIME_INSTANCES_2=$(cat ${SCRIPT_DIR}/sequences/delete.runtime.sequence.2 | grep -v '^#')
DELIVERY_INSTANCES=$(cat ${SCRIPT_DIR}/sequences/delete.delivery.sequence | grep -v '^#')

# delete用のブランチに切り替え
git checkout staff/for_delete

function destroy_pattern_instances() {
  local user_name=$1
  local pattern_instances=${@:2}

  for instance_dir in ${pattern_instances}; do
    destroy_pattern_instance ${user_name} ${instance_dir}
  done
}

for user in $USERS; do
    # cacheable_frontendで失敗想定なので、Error後もスクリプトを継続する
    set_staging_terraformer_credential ${user}
    set +e
    destroy_pattern_instances ${user} ${RUNTIME_INSTANCES_1[@]}
    set -e

    destroy_pattern_instances ${user} ${RUNTIME_INSTANCES_2[@]}

    set_delivery_terraformer_credential ${user}
    destroy_pattern_instances ${user} ${DELIVERY_INSTANCES[@]}
done
