provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::922032444791:role/SuzukiTerraformExecutionRole"
  }
}

module "redis" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/redis?ref=v0.2.1"

  replication_group_id          = "suzuki-redis-group"
  replication_group_description = "Suzuki Epona Redis Cluster"

  vpc_id = data.terraform_remote_state.staging_network.outputs.network.vpc_id

  cluster_mode_enabled = false

  number_cache_clusters = 1

  auth_token = "redis-password123"

  kms_key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/suzuki-common-encryption-key"].key_arn

  snapshot_retention_limit = 35
  snapshot_window          = "00:00-01:00"

  engine_version             = "5.0.6"
  node_type                  = "cache.t3.medium"
  automatic_failover_enabled = false

  maintenance_window = "sun:09:00-sun:10:00"

  apply_immediately = true

  family = "redis5.0"

  subnets = data.terraform_remote_state.staging_network.outputs.network.private_subnets

  # example parameters
  parameters = [
    {
      name  = "tcp-keepalive"
      value = 150 # default 300
    },
    {
      name  = "active-defrag-max-scan-fields"
      value = 2000 # default 1000
    }
  ]

  tags = {
    Owner              = "suzuki"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }
}
